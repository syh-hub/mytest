import pandas as pd
import requests
import json
import pymongo
import csv
import time
import sys


class Logger(object):
    def __init__(self, filename='default.log', stream=sys.stdout):
        self.terminal = stream
        self.log = open(filename, 'a',encoding='utf-8')

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)

    def flush(self):
        pass


sys.stdout = Logger('result.txt', sys.stdout)


#读取csv文件
# df = pd.read_csv("part.csv")

with open('extract.csv','r',encoding='utf-8') as csvfile:
    reader = csv.DictReader(csvfile)
    column = [row['question'] for row in reader]

# for i in range(0,len(column)):
#     print(column[i])


# print(type(column))
# print(len(column))
# print(column[0])

url = "https://ai.diwork.com/aiapigeteway/question_match/top_similar_question_annoy/?access_token=48811bd278c0b5c23a1f890324ea4a2e"


headers = {
    'user-agent':'ApiPOST Runtime +https://www.apipost.cn',
    'content-type':'application/json'
}

for j in range(0,len(column)):
    data = {
        "question": column[j],
        "table_name_list": [
            "repo-glo",
            "repo-cus-hr_cloud-qyic8c7o",
            "repo-cus-nccloud",
            "repo-cus-hr_cloud",
            "repo-cus-diworkfaq-qyic8c7o",
            "repo-cus-bipfaq",
            "repo-cus-diwork",
            "repo-glo-bipfaq",
            "repo-cus-diwork-qyic8c7o",
            "repo-glo-bjy",
            "repo-glo-pneumonia"
        ],
        "top_k": 10
    }
    response = requests.post(url=url, headers=headers, json=data)
    # 变成字典格式
    jsondata = json.loads(response.text)
    # 取到字典中的value字段
    dict1 = jsondata.get('value')
    # 连接数据库
    host = '10.3.2.251'
    client = pymongo.MongoClient(host, 3310)
    db = client.admin
    db.authenticate("ro_all_db", "eruij6g98_35", mechanism='SCRAM-SHA-1')
    my_db = client['nccloud-robot-brain']

    # length = len(dict1)
    # print(length)
    print(column[j])
    for i in range(0, 10):
        # 选择value字段中列表的哪一个
        dict2 = dict1[i]
        # 查询的两个变量
        table = dict2.get('table_name')
        qid = dict2.get('_id')
        index = dict2.get('idx_in_question')
        similarity = dict2.get('similarity')
        # 定位所在集合
        collection = my_db[table]
        # 从所在集合中查找问题id
        myquery = {"_id": qid}
        mydoc = collection.find(myquery, {"_id": 0, "question": 1, "similar_question": 1})
        array = list(mydoc)
        # print(array[0].keys())
        # print(type(array[0]))
        #字典的第一列
        if index==0:
            first = array[0]['question']
            print(first,',',similarity,sep='')
        else:
            second = array[0]['similar_question']
            print(second[index-1],',',similarity,sep='')

        # print(array[0]['question'], index, similarity)
        # print(type(array))

        # for x in mydoc:
        #     # print(type(x))
        #     print(x, index, similarity)
    print('\n')
    time.sleep(2)







